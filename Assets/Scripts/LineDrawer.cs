﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
    private LineRenderer lineRend;
    private List<Vector3> positions;

    void Awake()
    {
        lineRend = gameObject.GetComponent<LineRenderer>();
    }

    void Update()
    {
        if (!WaypointManager.WaypointsExists())
        {
            return;
        }
        DrawLine();
    }

    private void DrawLine()
    {
        positions = WaypointManager.GetWaypointsPositions();
        lineRend.positionCount = positions.Count + 1;
        lineRend.SetPosition(0, Unit.GetUnitPosition());
        for (int i = 0; i < positions.Count; i++)
        {
            lineRend.SetPosition(i + 1, positions[i]);
        }
    }
}
