﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseResume : MonoBehaviour
{
    [SerializeField]
    private Button pause;

    [SerializeField]
    private Button resume;

    private static bool paused;

    private void Start()
    {
        paused = false;
        pause.gameObject.SetActive(true);
        resume.gameObject.SetActive(false);
    }

    public void Pause()
    {
        paused = true;
        Time.timeScale = 0;
        pause.gameObject.SetActive(false);
        resume.gameObject.SetActive(true);
    }

    public void Resume()
    {
        paused = false;
        Time.timeScale = 1;
        pause.gameObject.SetActive(true);
        resume.gameObject.SetActive(false);
    }

    public static bool GameIsPaused()
    {
        return paused;
    }
}
