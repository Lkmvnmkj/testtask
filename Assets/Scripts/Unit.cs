﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    public delegate void ArriveToWaypoint();
    public static event ArriveToWaypoint OnArrive;

    [SerializeField]
    private float speed = 2;

    [SerializeField]
    private float rotationSpeed = 15;

    private static Transform unitTransform;

    private void Awake()
    {
        unitTransform = gameObject.GetComponent<Transform>();
    }

    void Update()
    {
        if (WaypointManager.WaypointsExists())
        {
            RotateUnit();
            transform.position = Vector2.MoveTowards(transform.position, WaypointManager.GetTargetPosition(), Time.deltaTime * speed);
            if (new Vector2(transform.position.x, transform.position.y) == WaypointManager.GetTargetPosition())
            {
                OnArrive?.Invoke();
            }
        }
    }

    private void RotateUnit()
    {
        Vector3 targetPosition = new Vector3(WaypointManager.GetTargetPosition().x, WaypointManager.GetTargetPosition().y, 0);
        Vector3 targetDirection = targetPosition - transform.position;
        var newDirection = Quaternion.LookRotation(Vector3.forward, targetDirection);
        transform.rotation = Quaternion.Slerp(transform.rotation, newDirection, rotationSpeed * Time.deltaTime);
    }

    public static Vector3 GetUnitPosition()
    {
        return unitTransform.position;
    }
}
