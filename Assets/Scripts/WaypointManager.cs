﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class WaypointManager : MonoBehaviour, IPointerDownHandler
{
    [SerializeField]
    private GameObject waypoint;

    private Camera cam;
    private static List<Transform> waypoints;

    void Awake()
    {
        cam = Camera.main;
        waypoints = new List<Transform>();
    }

    void Start()
    {
        Unit.OnArrive += DeleteWaypoint;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (PauseResume.GameIsPaused())
        {
            return;
        }
        Vector2 pos = new Vector2(cam.ScreenToWorldPoint(eventData.position).x, cam.ScreenToWorldPoint(eventData.position).y);
        GameObject go = Instantiate(waypoint, pos, Quaternion.identity);
        waypoints.Add(go.transform);
    }

    public static Vector2 GetTargetPosition()
    {
        if (waypoints.Count < 1)
        {
            return Vector2.zero;
        }
        return waypoints[0].position;
    }

    public static List<Vector3> GetWaypointsPositions()
    {
        List<Vector3> positions = new List<Vector3>();
        foreach(Transform t in waypoints)
        {
            positions.Add(t.position);
        }
        return positions;
    }

    public static bool WaypointsExists()
    {
        if (waypoints.Count < 1)
        {
            return false;
        }
        return true;
    }

    private void DeleteWaypoint()
    {
        Destroy(waypoints[0].gameObject);
        waypoints.RemoveAt(0);
    }
}